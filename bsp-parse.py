# Much of the information here was taken from (here)[https://developer.valvesoftware.com/wiki/Source_BSP_File_Format].

import struct

class STRING():
    def __init__(self, n):
        self.n = n
    def _format(self, inp):
        return inp[:n], inp[n:]

class ARRAY1D():
    def __init__(self, t, n):
        self.type = t
        self.n = n
    def _format(self, inp):
        res = []
        for _ in range(self.n):
            tmp = self.type._format(self.type, inp)
            x, inp = tmp
            res.append(x)
        return res, inp

class SHORT:
    def _format(self, inp):
        size = struct.calcsize("h")
        return struct.unpack("h", inp[:size])[0], inp[size:]
class USHORT:
    def _format(self, inp):
        size = struct.calcsize("H")
        return struct.unpack("H", inp[:size])[0], inp[size:]

class INT:
    def _format(self, inp):
        size = struct.calcsize("i")
        return struct.unpack("i", inp[:size])[0], inp[size:]
class UINT:
    def _format(self, inp):
        size = struct.calcsize("I")
        return struct.unpack("I", inp[:size])[0], inp[size:]

class LONG:
    def _format(self, inp):
        size = struct.calcsize("l")
        return struct.unpack("l", inp[:size])[0], inp[size:]
class ULONG:
    def _format(self, inp):
        size = struct.calcsize("L")
        return struct.unpack("L", inp[:size])[0], inp[size:]

class CHAR:
    def _format(self, inp):
        size = struct.calcsize("b")
        return struct.unpack("b", inp[:size])[0], inp[size:]
class UCHAR:
    def _format(self, inp):
        size = struct.calcsize("B")
        return struct.unpack("B", inp[:size])[0], inp[size:]

class FLOAT:
    def _format(self, inp):
        size = struct.calcsize("f")
        return struct.unpack("f", inp[:size])[0], inp[size:]
class DOUBLE:
    def _format(self, inp):
        size = struct.calcsize("d")
        return struct.unpack("d", inp[:size])[0], inp[size:]

class Structure():
    _format_ = []
    def __init__(self, *args, _copy=True):
        if len(args) == 1:
            if _copy:
                data = args[0][:]
            else:
                data = args[0]
            x, _ = self._format(data)
            self.__dict__.update(x.__dict__)
        elif len(args) == 0:
            return
        else:
            raise Exception("Use pack() for packing structures.")
    def _format(self, in_data):
        d = {}
        for f in self._format_:
            if type(f[1]) == type:
                res, in_data = f[1]._format(f[1], in_data)
            else:
                res, in_data = f[1]._format(in_data)
            d.update({f[0]: res})
        if type(self) == type:
            obj = self(_copy=False)
        else:
            obj = self.__class__(_copy=False)
        obj.__dict__.update(d)
        return obj, in_data

#############################################################################

# My constants
HEADERSIZE          = 1036

# Source constants
SURF_LIGHT = 0x1
SURF_SKY2D = 0x2
SURF_SKY   = 0x4
SURF_WARP  = 0x8
SURF_TRANS = 0x10
SURF_NOPORTAL = 0x20
SURF_TRIGGER = 0x40
SURF_NODRAW = 0x80
SURF_NOLIGHT = 0x400
SURF_BUMPLIGHT = 0x800
SURF_NOSHADOWS = 0x1000
SURF_NODECALS = 0x2000
SURF_HITBOX = 0x8000

HEADER_LUMPS        = 64

MAX_MAP_PLANES      = 65536
MAX_MAP_VERTS       = 65536
MAX_MAP_EDGES       = 256000
MAX_MAP_SURFEDGES   = 512000

LUMP_PLANES               = 1
LUMP_TEXDATA              = 2
LUMP_VERTEXES             = 3
LUMP_TEXINFO              = 6
LUMP_FACES                = 7
LUMP_EDGES                = 12
LUMP_SURFEDGES            = 13
LUMP_BRUSHSIDES           = 19
LUMP_TEXDATA_STRING_TABLE = 44
LUMP_TEXDATA_STRING_DATA  = 43

CONTENTS_EMPTY            = 0x00000000
CONTENTS_SOLID            = 0x00000001
CONTENTS_WINDOW           = 0x00000002
CONTENTS_AUX              = 0x00000004
CONTENTS_GRATE            = 0x00000008
CONTENTS_SLIME            = 0x00000010
CONTENTS_WATER            = 0x00000020
CONTENTS_MIST             = 0x00000040

class Surfedge(Structure):
    _format_ = [
        ("index", INT)
    ]

class Vector(Structure):
    _format_ = [
        ("x", FLOAT),
        ("y", FLOAT),
        ("z", FLOAT),
    ]
    # def __init__(self, x, y, z):
    #     self.x = x
    #     self.y = y
    #     self.z = z
    def __repr__(self):
        return "Vector3(x=%f, y=%f, z=%f)" % (self.x, self.y, self.z)
    def __iter__(self):
        yield self.x
        yield self.y
        yield self.z

class dbrushside_t(Structure):
    _format_ = [
        ("planenum", USHORT),
        ("texinfo", SHORT),
        ("dispinfo", SHORT),
        ("bevel", SHORT),
    ]

class dedge_t(Structure):
    _format_ = [
        ("v", ARRAY1D(USHORT, 2))
    ]

class dface_t(Structure):
    _format_ = [
        ("planenum", USHORT),                              # The plane number
        ("side", CHAR),                                    # Faces opposite to the node's plane direction
        ("onNode", CHAR),                                  # 1 if on node, 0 if on leaf
        ("firstedge", INT),                                # Index into surfedges
        ("numedges", SHORT),                               # Number of surfedges
        ("texinfo", SHORT),                                # Texture info
        ("dispinfo", SHORT),                               # Displacement info
        ("surfaceFogVolumeID", SHORT),                     # ?
        ("styles", ARRAY1D(CHAR, 4)),                      # Switchable lighting info
        ("lightofs", INT),                                 # Offset into lightmap lump
        ("area", FLOAT),                                   # Face area in units^2
        ("LightmapTextureMinsInLuxels", ARRAY1D(INT, 2)),  # Texture lighting info
        ("LightmapTextureSizeInLuxels", ARRAY1D(INT, 2)),  # Texture lighting info
        ("origFace", INT),                                 # Original face this was split from
        ("numPrims", USHORT),                              # Primitives
        ("firstPrimID", USHORT),                           #
        ("smoothingGroups", UINT),                         # Lightmap smoothing group
    ]

class texinfo_t(Structure):
    _format_ = [
        ("textureVecs", ARRAY1D(FLOAT, 4 * 2)),
        ("lightmapVecs", ARRAY1D(FLOAT, 4 * 2)),
        ("flags", INT),
        ("texdata", INT),
    ]

class dplane_t(Structure):
    """From developer.valvesoftware.com:
>  Mathematically, the plane is described by the set of points (x, y, z) which satisfy the equation:
>  
>  Ax + By + Cz = D
> 
>  where A, B, and C are given by the components normal.x, normal.y and normal.z,
>  and D is dist. Each plane is infinite in extent, and divides the whole of the map coordinate volume
>  into three pieces, on the plane (F=0), in front of the plane (F>0), and behind the plane (F<0).
"""
    _format_ = [
        ("normal", Vector),
        ("dist", FLOAT),
        ("type", INT),
    ]

class dbrush_t(Structure):
    _format_ = [
        ("firstside", INT),
        ("numsides", INT),
        ("contents", INT),
    ]

class lump_t(Structure):
    _format_ = [
        ("fileofs", INT),
        ("filelen", INT),
        ("version", INT),
        ("fourCC", ARRAY1D(CHAR, 4)),
    ]

class dheader_t(Structure):
    _format_ = [
        ("ident", INT),
        ("version", INT),
        ("lumps", ARRAY1D(lump_t, HEADER_LUMPS)),
        ("mapRevision", INT),
    ]

class dtexdata_t(Structure):
    _format_ = [
        ("reflectivity", Vector),
        ("nameStringTableID", INT),  # Index into TexdataStringTable
        ("width", INT),
        ("height", INT),
        ("view_width", INT),
        ("view_height", INT),
    ]

class TextureDataStringTableElement(Structure):
    # whoops a long name
    _format_ = [
        ("index", INT),
    ]

# TODO: Oh god
"""struct StaticPropLump_t
{
	// v4
	Vector          Origin;            // origin
	QAngle          Angles;            // orientation (pitch roll yaw)
	
	// v4
	unsigned short  PropType;          // index into model name dictionary
	unsigned short  FirstLeaf;         // index into leaf array
	unsigned short  LeafCount;
	unsigned char   Solid;             // solidity type
	unsigned char   Flags;
	int             Skin;              // model skin numbers
	float           FadeMinDist;
	float           FadeMaxDist;
	Vector          LightingOrigin;    // for lighting
	// since v5
	float           ForcedFadeScale;   // fade distance scale
	// v6 and v7 only
	unsigned short  MinDXLevel;        // minimum DirectX version to be visible
	unsigned short  MaxDXLevel;        // maximum DirectX version to be visible
	// since v8
	unsigned char   MinCPULevel;
	unsigned char   MaxCPULevel;
	unsigned char   MinGPULevel;
	unsigned char   MaxGPULevel;
	// since v7
	color32         DiffuseModulation; // per instance color and alpha modulation
	// v9 and v10 only
	bool            DisableX360;       // if true, don't show on XBox 360 (4-bytes long)
	// since v10
	unisgned int    FlagsEx;           // Further bitflags.
	// since v11
	float           UniformScale;      // Prop scale
};"""

class BSPFile():
    def __init__(self, raw):
        if data[:4] != b"VBSP":
            raise ValueError("Invalid magic! Should be 'VBSP', got '%s'" % (data[:4].decode("latin1")))
        self.raw = raw
        self.struct = dheader_t(raw[:HEADERSIZE])
        
        self.lumps = {}
        lumpid = 0
        for i in self.struct.lumps:
            if i.filelen != 0:
                self.lumps.update({lumpid: Lump(
                    lumpid,
                    self.raw[i.fileofs:(i.fileofs+i.filelen)]
                )})
            lumpid += 1

        self.version     = self.struct.version
        self.mapRevision = self.struct.mapRevision

        self.verify()

    def verify(self):
        return # idk why it borked
    
        lumpid = 0
        for lump_header in self.struct.lumps:
            if self.lumps.get(lumpid):
                print(len(self.lumps[lumpid].raw), lump_header.filelen)
                assert len(self.lumps[lumpid].raw) == lump_header.filelen, Exception("Lump length + offset exceeds file size")
            lumpid += 1

    def get_geometry(self, limit=None):
        geom = []
        abcd = len(self.lumps[LUMP_PLANES].contents)
        # print(abcd)
        L_edges     = self.lumps[LUMP_EDGES].contents
        L_surfedges = self.lumps[LUMP_SURFEDGES].contents
        L_verts     = self.lumps[LUMP_VERTEXES].contents
        L_faces     = self.lumps[LUMP_FACES].contents

        center_of_model = [0, 0, 0]
        for i in L_verts:
            center_of_model[0] += i.x
            center_of_model[1] += i.y
            center_of_model[2] += i.z

        center_of_model[0] /= len(L_verts)
        center_of_model[1] /= len(L_verts)
        center_of_model[2] /= len(L_verts)

        c = 0
        for face in L_faces:
            if c % 500 == 0:
                print(c / len(L_faces) * 100, "%")
            polygon = []
            # print("firstedge", face.firstedge, "numedges", face.numedges)
            for surfedge_i in range(face.firstedge, face.firstedge + face.numedges):
                edge = L_edges[abs(L_surfedges[surfedge_i].index)]
                if L_surfedges[surfedge_i].index < 0:
                    polygon += [L_verts[edge.v[1]], L_verts[edge.v[0]]]
                else:
                    polygon += [L_verts[edge.v[0]], L_verts[edge.v[1]]]

            # print(polygon)
            if len(polygon) > 0:
                center_of_polygon = [0, 0, 0]

                for vertex in polygon:
                    center_of_polygon[0] += vertex.x
                    center_of_polygon[1] += vertex.y
                    center_of_polygon[2] += vertex.z

                center_of_polygon[0] /= len(polygon)
                center_of_polygon[1] /= len(polygon)
                center_of_polygon[2] /= len(polygon)

                tris = []
                for vertex_i in range(len(polygon)):
                    tmp = polygon[(vertex_i + 1) % len(polygon)]
                    tris.append((
                        (
                            polygon[vertex_i].x - center_of_model[0],
                            polygon[vertex_i].y - center_of_model[1],
                            polygon[vertex_i].z - center_of_model[2]
                        ),
                        (
                            center_of_polygon[0] - center_of_model[0],
                            center_of_polygon[1] - center_of_model[1],
                            center_of_polygon[2] - center_of_model[2]
                        ),
                        (
                            tmp.x - center_of_model[0],
                            tmp.y - center_of_model[1],
                            tmp.z - center_of_model[2]
                        ),
                    ))

                geom += tris
            if limit:
                if len(geom) > limit:
                    break

            c += 1
        
        return geom

def list_find(l, i):
    try:
        return l.index(i)
    except ValueError:
        return -1

def tris2obj(geom, fn):
    """Turns a triangle list into an .OBJ file.
geom - The triangle list
fn   - Path to resulting obj file

The `geom` array needs to be in the following format:
  [
     [[v1x, v1y, v1z], [v2x, v2y, v2z], [v3x, v3y, v3z]],      # Tri 1
     [[v1x, v1y, v1z], [v2x, v2y, v2z], [v3x, v3y, v3z]],      # Tri 2
     .
     .
     .
     [[v1x, v1y, v1z], [v2x, v2y, v2z], [v3x, v3y, v3z]],      # Tri N
  ]
"""
    # Stage 1
    verts = []
    faces = []
    try:
        c = 0
        for tri in geom:
            if c % ((len(geom)//25) if len(geom) > 25 else (len(geom) / 25)) == 0:
                print(c / len(geom) * 100, "% [", c, "]", sep="")

            tri_v0_location = list_find(verts, tri[0])
            if tri_v0_location == -1:
                verts.append(tri[0])
                tri_v0_location = len(verts) - 1
            tri_v1_location = list_find(verts, tri[1])
            if tri_v1_location == -1:
                verts.append(tri[1])
                tri_v1_location = len(verts) - 1
            tri_v2_location = list_find(verts, tri[2])
            if tri_v2_location == -1:
                verts.append(tri[2])
                tri_v2_location = len(verts) - 1

            faces.append((
                tri_v0_location + 1,
                tri_v1_location + 1,
                tri_v2_location + 1
            ))
            c += 1
    except KeyboardInterrupt:
        print("Interrupted, moving onto stage 2")

    # Stage 2
    res = "# bsp2obj output\n"
    res += "\n".join(["v %.8f %.8f %.8f" % i for i in verts])
    res += "\n"
    res += "\n".join(["f %d %d %d" % i for i in faces])

    # Stage 3
    with open(fn, "w") as f:
        f.write(res)

class Lump():
    def __init__(self, id, raw_data):
        self.raw      = raw_data
        self.id       = id
        self.contents = None
    
        self.__get_contents()

    def __get_contents(self):
        assert self.contents == None   # Call only during initialization
        if self.id == LUMP_PLANES:
            bytes_per_plane = 20
            self.contents = []
            for i in range(len(self.raw) // bytes_per_plane):
                self.contents.append(dplane_t(self.raw))
                self.raw = self.raw[bytes_per_plane:]
        elif self.id == LUMP_VERTEXES:
            bytes_per_vertex = 12
            self.contents = []
            for i in range(len(self.raw) // bytes_per_vertex):
                self.contents.append(Vector(self.raw))
                self.raw = self.raw[bytes_per_vertex:]
        elif self.id == LUMP_EDGES:
            bytes_per_edge = 4
            self.contents = []
            for i in range(len(self.raw) // bytes_per_edge):
                self.contents.append(dedge_t(self.raw))
                self.raw = self.raw[bytes_per_edge:]
        elif self.id == LUMP_SURFEDGES:
            bytes_per_edge = 4
            self.contents = []
            for i in range(len(self.raw) // bytes_per_edge):
                self.contents.append(Surfedge(self.raw))
                self.raw = self.raw[bytes_per_edge:]
        elif self.id == LUMP_FACES:
            bytes_per_edge = 56
            self.contents = []
            for i in range(len(self.raw) // bytes_per_edge):
                self.contents.append(dface_t(self.raw))
                self.raw = self.raw[bytes_per_edge:]
        elif self.id == LUMP_BRUSHSIDES:
            bytes_per_edge = 8
            self.contents = []
            for i in range(len(self.raw) // bytes_per_edge):
                self.contents.append(dbrushside_t(self.raw))
                self.raw = self.raw[bytes_per_edge:]
        elif self.id == LUMP_TEXINFO:
            """
u = tv0,0 * x + tv0,1 * y + tv0,2 * z + tv0,3

v = tv1,0 * x + tv1,1 * y + tv1,2 * z + tv1,3"""
            bytes_per_edge = 72
            self.contents = []
            for i in range(len(self.raw) // bytes_per_edge):
                self.contents.append(texinfo_t(self.raw))
                self.raw = self.raw[bytes_per_edge:]
        elif self.id == LUMP_TEXDATA:
            bytes_per_edge = 32
            self.contents = []
            for i in range(len(self.raw) // bytes_per_edge):
                self.contents.append(dtexdata_t(self.raw))
                self.raw = self.raw[bytes_per_edge:]
        elif self.id == LUMP_TEXDATA_STRING_TABLE:
            bytes_per_edge = 4
            self.contents = []
            for i in range(len(self.raw) // bytes_per_edge):
                self.contents.append(TextureDataStringTableElement(self.raw))
                self.raw = self.raw[bytes_per_edge:]
        elif self.id == LUMP_TEXDATA_STRING_DATA:
            self.contents = []
            x = ""
            for i in self.raw:
                if i == 0:
                    self.contents.append(x)
                    x = ""
                    continue
                x += bytes([i,]).decode("latin1")

#############################################################################

with open(input(".BSP file path: "), "rb") as f:
    data = f.read()
    
    BSP = BSPFile(data)  # BSPHEADER = dheader_t(data[:HEADERSIZE])
